var arrNumber = [];
function nhapSoDuong() {
  if (document.querySelector("#txt-so-n").value.trim() == "") {
    return;
  }
  var nhapSo = document.querySelector("#txt-so-n").value * 1;
  arrNumber.push(nhapSo);
  console.log("arrNumber: ", arrNumber);
  document.querySelector("#txt-so-n").value = "";
  document.querySelector(
    "#hien-thi"
  ).innerHTML = `<div><p>Array: ${arrNumber}</p></div>`;
}

// BÀI 1

function tongSoDuong(arr) {
  var tongSoDuong = 0;
  arr.forEach(function (number) {
    if (number > 0) {
      tongSoDuong += number;
    }
  });
  return tongSoDuong;
}

function btnTongSoDuong() {
  document.querySelector("#ket-qua-1").innerHTML = `<div>
  <p> Tổng số dương: ${tongSoDuong(arrNumber)}</p>
  </div>`;
}

// BÀI 2
function demSoDuong(arr) {
  var demSoDuong = 0;
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] > 0) {
      demSoDuong++;
    }
  }
  return demSoDuong;
}

function btnDemSoDuong() {
  document.querySelector("#ket-qua-2").innerHTML = `<div>
    <p> Số dương: ${demSoDuong(arrNumber)}</p>
    </div>`;
}

// BÀI 3
function btnSoNhoNhat() {
  var soNhoNhat = arrNumber[0];
  for (var i = 0; i < arrNumber.length; i++) {
    if (arrNumber[i] < soNhoNhat) {
      soNhoNhat = arrNumber[i];
    }
  }
  document.querySelector("#ket-qua-3").innerHTML = `<div>
    <p> Số nhỏ nhất: ${soNhoNhat}</p>
    </div>`;
}

// BÀI 4
function soDuongNhoNhat(arr) {
  var soDuongNhoNhat = -1;
  for (var i = 0; i < arr.length; i++) {
    if (soDuongNhoNhat == -1 || (soDuongNhoNhat > arr[i] && arr[i] > 0)) {
      soDuongNhoNhat = arr[i];
    }
  }
  return soDuongNhoNhat;
}

function btnSoDuongNhoNhat() {
  document.querySelector("#ket-qua-4").innerHTML = `<div>
    <p> Số dương nhỏ nhất: ${soDuongNhoNhat(arrNumber)}</p>
    </div>`;
}

// BÀI 5
function soChanCuoiCung(arr) {
  var soChanCuoiCung = -1;
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] % 2 == 0) {
      soChanCuoiCung = arr[i];
    }
  }
  return soChanCuoiCung;
}

function btnTimSoChanCuoiCung() {
  document.querySelector("#ket-qua-5").innerHTML = `<div>
    <p> Số chẵn cuối cùng: ${soChanCuoiCung(arrNumber)}</p>
    </div>`;
}

// BÀI 6
function btnDoiCho() {
  var viTriSo1 = document.querySelector("#txt-so-1").value * 1;
  var viTriSo2 = document.querySelector("#txt-so-2").value * 1;

  var viTriDoi = arrNumber[viTriSo1];
  arrNumber[viTriSo1] = arrNumber[viTriSo2];
  arrNumber[viTriSo2] = viTriDoi;

  document.querySelector("#ket-qua-6").innerHTML = `<div>
    <p> Mảng sau khi đổi chỗ: ${arrNumber}</p>
    </div>`;
}

// BÀI 7
function btnSapXep() {
  arrNumber.sort(function (a, b) {
    return a - b;
  });
  document.querySelector("#ket-qua-7").innerHTML = `<div>
        <p> Sắp xếp tăng dần: ${arrNumber}</p>
        </div>`;
}

// BÀI 8
function kiemTraSo(number) {
  var kiemTra = 1;
  for (var i = 2; i <= Math.sqrt(number); i++) {
    if (number % i === 0) {
      kiemTra = 0;
      break;
    }
  }
  return kiemTra;
}

function btnSoNguyenToDauTien() {
  var ketQua = "";
  for (var i = 1; i < arrNumber.length; i++) {
    var kiemTra = kiemTraSo(arrNumber[i]);
    if (kiemTra) {
      ketQua = arrNumber[i];
      break;
    } else {
      ketQua = -1;
    }
  }

  document.querySelector("#ket-qua-8").innerHTML = `<div>
    <p> Số nguyên tố đầu tiên: ${ketQua}</p>
    </div>`;
}

// BÀI 9
function demSoNguyen(arr) {
  var demSoNguyen = 0;
  for (var i = 0; i < arr.length; i++) {
    if (Number.isInteger(arr[i]) / 2) {
      demSoNguyen++;
    }
  }
  return demSoNguyen;
}

function btnDemSoNguyen() {
  document.querySelector("#ket-qua-9").innerHTML = `<div>
      <p> Số nguyên: ${demSoNguyen(arrNumber)}</p>
      </div>`;
}

// BÀI 10
function demSoAm(arr) {
  var demSoAm = 0;
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] < 0) {
      demSoAm++;
    }
  }
  return demSoAm;
}

function btnSoSanh() {
  if (demSoDuong(arrNumber) > demSoAm(arrNumber)) {
    document.querySelector("#ket-qua-10").innerHTML = `<div>
    <p> Số dương > Số âm</p>
    </div>`;
  } else if (demSoDuong(arrNumber) < demSoAm(arrNumber)) {
    document.querySelector("#ket-qua-10").innerHTML = `<div>
    <p> Số dương < Số âm</p>
    </div>`;
  } else {
    document.querySelector("#ket-qua-10").innerHTML = `<div>
    <p> Số dương = Số âm</p>
    </div>`;
  }
}
